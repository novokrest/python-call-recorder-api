#!/bin/bash

TOOLS_DIR=tools

set -x
mkdir -p $TOOLS_DIR
curl http://central.maven.org/maven2/io/swagger/codegen/v3/swagger-codegen-cli/3.0.8/swagger-codegen-cli-3.0.8.jar -o $TOOLS_DIR/swagger-codegen-cli.jar

