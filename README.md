# call-recorder-api

Python package to use [Call Recorder API](docs/api.pdf)

## Requirements.

Python 2.7 and 3.4+

## Build

You can build package by:
```sh
python3 setup.py sdist bdist_wheel
```

and then upload to Pypi index:
```sh
python3 -m twine upload dist/*
```

## Installation & Usage
### pip install

To install from Pypi:
```sh
pip install call-recorder-api
```

If the python package is hosted on Github, you can install directly from Github

```sh
pip install git+https://bitbucket.org/novokrest/python-call-recorder-api.git
```
(you may need to run `pip` with root permission: `sudo pip install git+https://bitbucket.org/novokrest/python-call-recorder-api.git`)

Then import the package:
```python
import call_recorder_api 
```

### Setuptools

Install via [Setuptools](http://pypi.python.org/pypi/setuptools).

```sh
cd src && python setup.py install --user && cd -
```
(or `sudo python setup.py install` to install the package for all users)

Then import the package:
```python
import call_recorder_api
```

## Getting Started

Please follow the [installation procedure](#installation--usage) and then run the following:

```python
from __future__ import print_function
import time
import call_recorder_api
from call_recorder_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = call_recorder_api.DefaultApi(call_recorder_api.ApiClient(configuration))
api_key = 'api_key_example' # str | 
amount = 789 # int | 
receipt = 'receipt_example' # str | 
product_id = 789 # int | 
device_type = call_recorder_api.DeviceType() # DeviceType | 

try:
    api_response = api_instance.buy_credits_post(api_key, amount, receipt, product_id, device_type)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->buy_credits_post: %s\n" % e)

# create an instance of the API class
api_instance = call_recorder_api.DefaultApi(call_recorder_api.ApiClient(configuration))
api_key = 'api_key_example' # str | 
id = 789 # int | 

try:
    api_response = api_instance.clone_file_post(api_key, id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->clone_file_post: %s\n" % e)

# create an instance of the API class
api_instance = call_recorder_api.DefaultApi(call_recorder_api.ApiClient(configuration))
api_key = 'api_key_example' # str | 
file = 'file_example' # str | 
data = call_recorder_api.CreateFileData() # CreateFileData | 

try:
    api_response = api_instance.create_file_post(api_key, file, data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->create_file_post: %s\n" % e)

# create an instance of the API class
api_instance = call_recorder_api.DefaultApi(call_recorder_api.ApiClient(configuration))
api_key = 'api_key_example' # str | 
name = 'name_example' # str | 
_pass = '_pass_example' # str | 

try:
    api_response = api_instance.create_folder_post(api_key, name, _pass)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->create_folder_post: %s\n" % e)

# create an instance of the API class
api_instance = call_recorder_api.DefaultApi(call_recorder_api.ApiClient(configuration))
api_key = 'api_key_example' # str | 
ids = [56] # list[int] | 
action = 'action_example' # str | 

try:
    api_response = api_instance.delete_files_post(api_key, ids, action)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->delete_files_post: %s\n" % e)

# create an instance of the API class
api_instance = call_recorder_api.DefaultApi(call_recorder_api.ApiClient(configuration))
api_key = 'api_key_example' # str | 
id = 789 # int | 
move_to = 789 # int | 

try:
    api_response = api_instance.delete_folder_post(api_key, id, move_to)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->delete_folder_post: %s\n" % e)

# create an instance of the API class
api_instance = call_recorder_api.DefaultApi(call_recorder_api.ApiClient(configuration))
api_key = 'api_key_example' # str | 
ids = [56] # list[int] | 
parent_id = 789 # int | 

try:
    api_response = api_instance.delete_meta_files_post(api_key, ids, parent_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->delete_meta_files_post: %s\n" % e)

# create an instance of the API class
api_instance = call_recorder_api.DefaultApi(call_recorder_api.ApiClient(configuration))
api_key = 'api_key_example' # str | 
page = 'page_example' # str | 
folder_id = 789 # int | 
source = 'source_example' # str | 
_pass = '_pass_example' # str | 
reminder = true # bool | 
q = 'q_example' # str | 
id = 789 # int | 
op = 'op_example' # str | 

try:
    api_response = api_instance.get_files_post(api_key, page, folder_id, source, _pass, reminder, q, id, op)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_files_post: %s\n" % e)

# create an instance of the API class
api_instance = call_recorder_api.DefaultApi(call_recorder_api.ApiClient(configuration))
api_key = 'api_key_example' # str | 

try:
    api_response = api_instance.get_folders_post(api_key)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_folders_post: %s\n" % e)

# create an instance of the API class
api_instance = call_recorder_api.DefaultApi(call_recorder_api.ApiClient(configuration))
api_key = 'api_key_example' # str | 

try:
    api_response = api_instance.get_languages_post(api_key)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_languages_post: %s\n" % e)

# create an instance of the API class
api_instance = call_recorder_api.DefaultApi(call_recorder_api.ApiClient(configuration))
api_key = 'api_key_example' # str | 
parent_id = 789 # int | 

try:
    api_response = api_instance.get_meta_files_post(api_key, parent_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_meta_files_post: %s\n" % e)

# create an instance of the API class
api_instance = call_recorder_api.DefaultApi(call_recorder_api.ApiClient(configuration))
api_key = 'api_key_example' # str | 

try:
    api_response = api_instance.get_msgs_post(api_key)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_msgs_post: %s\n" % e)

# create an instance of the API class
api_instance = call_recorder_api.DefaultApi(call_recorder_api.ApiClient(configuration))
api_key = 'api_key_example' # str | 

try:
    api_response = api_instance.get_phones_post(api_key)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_phones_post: %s\n" % e)

# create an instance of the API class
api_instance = call_recorder_api.DefaultApi(call_recorder_api.ApiClient(configuration))
api_key = 'api_key_example' # str | 

try:
    api_response = api_instance.get_profile_post(api_key)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_profile_post: %s\n" % e)

# create an instance of the API class
api_instance = call_recorder_api.DefaultApi(call_recorder_api.ApiClient(configuration))
api_key = 'api_key_example' # str | 

try:
    api_response = api_instance.get_settings_post(api_key)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_settings_post: %s\n" % e)

# create an instance of the API class
api_instance = call_recorder_api.DefaultApi(call_recorder_api.ApiClient(configuration))
api_key = 'api_key_example' # str | 
language = 'language_example' # str | 

try:
    api_response = api_instance.get_translations_post(api_key, language)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_translations_post: %s\n" % e)

# create an instance of the API class
api_instance = call_recorder_api.DefaultApi(call_recorder_api.ApiClient(configuration))
api_key = 'api_key_example' # str | 
title = 'title_example' # str | 
body = 'body_example' # str | 
device_type = call_recorder_api.DeviceType() # DeviceType | 

try:
    api_response = api_instance.notify_user_custom_post(api_key, title, body, device_type)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->notify_user_custom_post: %s\n" % e)

# create an instance of the API class
api_instance = call_recorder_api.DefaultApi(call_recorder_api.ApiClient(configuration))
api_key = 'api_key_example' # str | 
id = 789 # int | 
folder_id = 789 # int | 

try:
    api_response = api_instance.recover_file_post(api_key, id, folder_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->recover_file_post: %s\n" % e)

# create an instance of the API class
api_instance = call_recorder_api.DefaultApi(call_recorder_api.ApiClient(configuration))
token = 'token_example' # str | 
phone = 'phone_example' # str | 

try:
    api_response = api_instance.register_phone_post(token, phone)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->register_phone_post: %s\n" % e)

# create an instance of the API class
api_instance = call_recorder_api.DefaultApi(call_recorder_api.ApiClient(configuration))
api_key = 'api_key_example' # str | 
device_token = 'device_token_example' # str | 
device_type = call_recorder_api.DeviceType() # DeviceType | 

try:
    api_response = api_instance.update_device_token_post(api_key, device_token, device_type)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->update_device_token_post: %s\n" % e)

# create an instance of the API class
api_instance = call_recorder_api.DefaultApi(call_recorder_api.ApiClient(configuration))
api_key = 'api_key_example' # str | 
id = 789 # int | 
f_name = 'f_name_example' # str | 
l_name = 'l_name_example' # str | 
notes = 'notes_example' # str | 
email = 'email_example' # str | 
phone = 'phone_example' # str | 
tags = 'tags_example' # str | 
folder_id = 789 # int | 
name = 'name_example' # str | 
remind_days = 'remind_days_example' # str | 
remind_date = '2013-10-20T19:20:30+01:00' # datetime | 

try:
    api_response = api_instance.update_file_post(api_key, id, f_name, l_name, notes, email, phone, tags, folder_id, name, remind_days, remind_date)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->update_file_post: %s\n" % e)

# create an instance of the API class
api_instance = call_recorder_api.DefaultApi(call_recorder_api.ApiClient(configuration))
api_key = 'api_key_example' # str | 
id = 789 # int | 
name = 'name_example' # str | 
_pass = '_pass_example' # str | 
is_private = true # bool | 

try:
    api_response = api_instance.update_folder_post(api_key, id, name, _pass, is_private)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->update_folder_post: %s\n" % e)

# create an instance of the API class
api_instance = call_recorder_api.DefaultApi(call_recorder_api.ApiClient(configuration))
api_key = 'api_key_example' # str | 
folders = [call_recorder_api.UpdateOrderRequestFolders()] # list[UpdateOrderRequestFolders] | 

try:
    api_response = api_instance.update_order_post(api_key, folders)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->update_order_post: %s\n" % e)

# create an instance of the API class
api_instance = call_recorder_api.DefaultApi(call_recorder_api.ApiClient(configuration))
api_key = 'api_key_example' # str | 
file = 'file_example' # str | 

try:
    api_response = api_instance.update_profile_img_post(api_key, file)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->update_profile_img_post: %s\n" % e)

# create an instance of the API class
api_instance = call_recorder_api.DefaultApi(call_recorder_api.ApiClient(configuration))
api_key = 'api_key_example' # str | 
data = call_recorder_api.UpdateProfileRequestData() # UpdateProfileRequestData | 

try:
    api_response = api_instance.update_profile_post(api_key, data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->update_profile_post: %s\n" % e)

# create an instance of the API class
api_instance = call_recorder_api.DefaultApi(call_recorder_api.ApiClient(configuration))
api_key = 'api_key_example' # str | 
play_beep = call_recorder_api.PlayBeep() # PlayBeep | 
files_permission = call_recorder_api.FilesPermission() # FilesPermission | 

try:
    api_response = api_instance.update_settings_post(api_key, play_beep, files_permission)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->update_settings_post: %s\n" % e)

# create an instance of the API class
api_instance = call_recorder_api.DefaultApi(call_recorder_api.ApiClient(configuration))
api_key = 'api_key_example' # str | 
id = 789 # int | 
star = 56 # int | 
type = 'type_example' # str | 

try:
    api_response = api_instance.update_star_post(api_key, id, star, type)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->update_star_post: %s\n" % e)

# create an instance of the API class
api_instance = call_recorder_api.DefaultApi(call_recorder_api.ApiClient(configuration))
api_key = 'api_key_example' # str | 
app = call_recorder_api.App() # App | 
timezone = 'timezone_example' # str | 

try:
    api_response = api_instance.update_user_post(api_key, app, timezone)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->update_user_post: %s\n" % e)

# create an instance of the API class
api_instance = call_recorder_api.DefaultApi(call_recorder_api.ApiClient(configuration))
api_key = 'api_key_example' # str | 
file = 'file_example' # str | 
name = 'name_example' # str | 
parent_id = 789 # int | 
id = 789 # int | 

try:
    api_response = api_instance.upload_meta_file_post(api_key, file, name, parent_id, id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->upload_meta_file_post: %s\n" % e)

# create an instance of the API class
api_instance = call_recorder_api.DefaultApi(call_recorder_api.ApiClient(configuration))
api_key = 'api_key_example' # str | 
id = 789 # int | 
_pass = '_pass_example' # str | 

try:
    api_response = api_instance.verify_folder_pass_post(api_key, id, _pass)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->verify_folder_pass_post: %s\n" % e)

# create an instance of the API class
api_instance = call_recorder_api.DefaultApi(call_recorder_api.ApiClient(configuration))
token = 'token_example' # str | 
phone = 'phone_example' # str | 
code = 'code_example' # str | 
mcc = 'mcc_example' # str | 
app = call_recorder_api.App() # App | 
device_token = 'device_token_example' # str | 
device_id = 'device_id_example' # str | 
device_type = call_recorder_api.DeviceType() # DeviceType | 
time_zone = 'time_zone_example' # str | 

try:
    api_response = api_instance.verify_phone_post(token, phone, code, mcc, app, device_token, device_id, device_type, time_zone)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->verify_phone_post: %s\n" % e)
```

## Documentation for API Endpoints

All URIs are relative to *https://app2.virtualbrix.net/rapi*

[**API specification**](docs/spec.yml)

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*DefaultApi* | [**buy_credits_post**](docs/DefaultApi.md#buy_credits_post) | **POST** /buy_credits | 
*DefaultApi* | [**clone_file_post**](docs/DefaultApi.md#clone_file_post) | **POST** /clone_file | 
*DefaultApi* | [**create_file_post**](docs/DefaultApi.md#create_file_post) | **POST** /create_file | 
*DefaultApi* | [**create_folder_post**](docs/DefaultApi.md#create_folder_post) | **POST** /create_folder | 
*DefaultApi* | [**delete_files_post**](docs/DefaultApi.md#delete_files_post) | **POST** /delete_files | 
*DefaultApi* | [**delete_folder_post**](docs/DefaultApi.md#delete_folder_post) | **POST** /delete_folder | 
*DefaultApi* | [**delete_meta_files_post**](docs/DefaultApi.md#delete_meta_files_post) | **POST** /delete_meta_files | 
*DefaultApi* | [**get_files_post**](docs/DefaultApi.md#get_files_post) | **POST** /get_files | 
*DefaultApi* | [**get_folders_post**](docs/DefaultApi.md#get_folders_post) | **POST** /get_folders | 
*DefaultApi* | [**get_languages_post**](docs/DefaultApi.md#get_languages_post) | **POST** /get_languages | 
*DefaultApi* | [**get_meta_files_post**](docs/DefaultApi.md#get_meta_files_post) | **POST** /get_meta_files | 
*DefaultApi* | [**get_msgs_post**](docs/DefaultApi.md#get_msgs_post) | **POST** /get_msgs | 
*DefaultApi* | [**get_phones_post**](docs/DefaultApi.md#get_phones_post) | **POST** /get_phones | 
*DefaultApi* | [**get_profile_post**](docs/DefaultApi.md#get_profile_post) | **POST** /get_profile | 
*DefaultApi* | [**get_settings_post**](docs/DefaultApi.md#get_settings_post) | **POST** /get_settings | 
*DefaultApi* | [**get_translations_post**](docs/DefaultApi.md#get_translations_post) | **POST** /get_translations | 
*DefaultApi* | [**notify_user_custom_post**](docs/DefaultApi.md#notify_user_custom_post) | **POST** /notify_user_custom | 
*DefaultApi* | [**recover_file_post**](docs/DefaultApi.md#recover_file_post) | **POST** /recover_file | 
*DefaultApi* | [**register_phone_post**](docs/DefaultApi.md#register_phone_post) | **POST** /register_phone | 
*DefaultApi* | [**update_device_token_post**](docs/DefaultApi.md#update_device_token_post) | **POST** /update_device_token | 
*DefaultApi* | [**update_file_post**](docs/DefaultApi.md#update_file_post) | **POST** /update_file | 
*DefaultApi* | [**update_folder_post**](docs/DefaultApi.md#update_folder_post) | **POST** /update_folder | 
*DefaultApi* | [**update_order_post**](docs/DefaultApi.md#update_order_post) | **POST** /update_order | 
*DefaultApi* | [**update_profile_img_post**](docs/DefaultApi.md#update_profile_img_post) | **POST** /update_profile_img | 
*DefaultApi* | [**update_profile_post**](docs/DefaultApi.md#update_profile_post) | **POST** /update_profile | 
*DefaultApi* | [**update_settings_post**](docs/DefaultApi.md#update_settings_post) | **POST** /update_settings | 
*DefaultApi* | [**update_star_post**](docs/DefaultApi.md#update_star_post) | **POST** /update_star | 
*DefaultApi* | [**update_user_post**](docs/DefaultApi.md#update_user_post) | **POST** /update_user | 
*DefaultApi* | [**upload_meta_file_post**](docs/DefaultApi.md#upload_meta_file_post) | **POST** /upload_meta_file | 
*DefaultApi* | [**verify_folder_pass_post**](docs/DefaultApi.md#verify_folder_pass_post) | **POST** /verify_folder_pass | 
*DefaultApi* | [**verify_phone_post**](docs/DefaultApi.md#verify_phone_post) | **POST** /verify_phone | 

## Documentation For Models

 - [App](docs/App.md)
 - [BuyCreditsRequest](docs/BuyCreditsRequest.md)
 - [BuyCreditsResponse](docs/BuyCreditsResponse.md)
 - [CloneFileRequest](docs/CloneFileRequest.md)
 - [CloneFileResponse](docs/CloneFileResponse.md)
 - [CreateFileData](docs/CreateFileData.md)
 - [CreateFileRequest](docs/CreateFileRequest.md)
 - [CreateFileResponse](docs/CreateFileResponse.md)
 - [CreateFolderRequest](docs/CreateFolderRequest.md)
 - [CreateFolderResponse](docs/CreateFolderResponse.md)
 - [DeleteFilesRequest](docs/DeleteFilesRequest.md)
 - [DeleteFilesResponse](docs/DeleteFilesResponse.md)
 - [DeleteFolderRequest](docs/DeleteFolderRequest.md)
 - [DeleteFolderResponse](docs/DeleteFolderResponse.md)
 - [DeleteMetaFilesRequest](docs/DeleteMetaFilesRequest.md)
 - [DeleteMetaFilesResponse](docs/DeleteMetaFilesResponse.md)
 - [DeviceType](docs/DeviceType.md)
 - [FilesPermission](docs/FilesPermission.md)
 - [GetFilesRequest](docs/GetFilesRequest.md)
 - [GetFilesResponse](docs/GetFilesResponse.md)
 - [GetFilesResponseFiles](docs/GetFilesResponseFiles.md)
 - [GetFoldersRequest](docs/GetFoldersRequest.md)
 - [GetFoldersResponse](docs/GetFoldersResponse.md)
 - [GetFoldersResponseFolders](docs/GetFoldersResponseFolders.md)
 - [GetLanguagesRequest](docs/GetLanguagesRequest.md)
 - [GetLanguagesResponse](docs/GetLanguagesResponse.md)
 - [GetLanguagesResponseLanguages](docs/GetLanguagesResponseLanguages.md)
 - [GetMessagesRequest](docs/GetMessagesRequest.md)
 - [GetMessagesResponse](docs/GetMessagesResponse.md)
 - [GetMessagesResponseMsgs](docs/GetMessagesResponseMsgs.md)
 - [GetMetaFilesRequest](docs/GetMetaFilesRequest.md)
 - [GetMetaFilesResponse](docs/GetMetaFilesResponse.md)
 - [GetMetaFilesResponseMetaFiles](docs/GetMetaFilesResponseMetaFiles.md)
 - [GetPhonesRequest](docs/GetPhonesRequest.md)
 - [GetPhonesResponse](docs/GetPhonesResponse.md)
 - [GetPhonesResponseInner](docs/GetPhonesResponseInner.md)
 - [GetProfileRequest](docs/GetProfileRequest.md)
 - [GetProfileResponse](docs/GetProfileResponse.md)
 - [GetProfileResponseProfile](docs/GetProfileResponseProfile.md)
 - [GetSettingsRequest](docs/GetSettingsRequest.md)
 - [GetSettingsResponse](docs/GetSettingsResponse.md)
 - [GetSettingsResponseSettings](docs/GetSettingsResponseSettings.md)
 - [GetTranslationsRequest](docs/GetTranslationsRequest.md)
 - [GetTranslationsResponse](docs/GetTranslationsResponse.md)
 - [NotifyUserRequest](docs/NotifyUserRequest.md)
 - [NotifyUserResponse](docs/NotifyUserResponse.md)
 - [PlayBeep](docs/PlayBeep.md)
 - [RecoverFileRequest](docs/RecoverFileRequest.md)
 - [RecoverFileResponse](docs/RecoverFileResponse.md)
 - [RegisterPhoneRequest](docs/RegisterPhoneRequest.md)
 - [RegisterPhoneResponse](docs/RegisterPhoneResponse.md)
 - [UpdateDeviceTokenRequest](docs/UpdateDeviceTokenRequest.md)
 - [UpdateDeviceTokenResponse](docs/UpdateDeviceTokenResponse.md)
 - [UpdateFileRequest](docs/UpdateFileRequest.md)
 - [UpdateFileResponse](docs/UpdateFileResponse.md)
 - [UpdateFolderRequest](docs/UpdateFolderRequest.md)
 - [UpdateFolderResponse](docs/UpdateFolderResponse.md)
 - [UpdateOrderRequest](docs/UpdateOrderRequest.md)
 - [UpdateOrderRequestFolders](docs/UpdateOrderRequestFolders.md)
 - [UpdateOrderResponse](docs/UpdateOrderResponse.md)
 - [UpdateProfileImgRequest](docs/UpdateProfileImgRequest.md)
 - [UpdateProfileImgResponse](docs/UpdateProfileImgResponse.md)
 - [UpdateProfileRequest](docs/UpdateProfileRequest.md)
 - [UpdateProfileRequestData](docs/UpdateProfileRequestData.md)
 - [UpdateProfileResponse](docs/UpdateProfileResponse.md)
 - [UpdateSettingsRequest](docs/UpdateSettingsRequest.md)
 - [UpdateSettingsResponse](docs/UpdateSettingsResponse.md)
 - [UpdateStarRequest](docs/UpdateStarRequest.md)
 - [UpdateStarResponse](docs/UpdateStarResponse.md)
 - [UpdateUserRequest](docs/UpdateUserRequest.md)
 - [UpdateUserResponse](docs/UpdateUserResponse.md)
 - [UploadMetaFileRequest](docs/UploadMetaFileRequest.md)
 - [UploadMetaFileResponse](docs/UploadMetaFileResponse.md)
 - [VerifyFolderPassRequest](docs/VerifyFolderPassRequest.md)
 - [VerifyFolderPassResponse](docs/VerifyFolderPassResponse.md)
 - [VerifyPhoneRequest](docs/VerifyPhoneRequest.md)
 - [VerifyPhoneResponse](docs/VerifyPhoneResponse.md)

## Author

Konstantin Novokreshchenov (novokrest013@gmail.com)
