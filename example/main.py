from __future__ import print_function
import time
import call_recorder_api
from call_recorder_api.configuration import Configuration
from call_recorder_api.rest import ApiException
from pprint import pprint

if __name__ == '__main__':
    config = Configuration()
    api_instance = call_recorder_api.DefaultApi(call_recorder_api.ApiClient(config))
    token = '55942ee3894f51000530894'
    phone = '+16463742122'

    try:
        api_response = api_instance.register_phone_post(token, phone)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->register_phone_post: %s\n" % e)
