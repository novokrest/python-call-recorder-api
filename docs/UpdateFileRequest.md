# UpdateFileRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**api_key** | **str** |  | 
**id** | **int** |  | [optional] 
**f_name** | **str** |  | [optional] 
**l_name** | **str** |  | [optional] 
**notes** | **str** |  | [optional] 
**email** | **str** |  | [optional] 
**phone** | **str** |  | [optional] 
**tags** | **str** |  | [optional] 
**folder_id** | **int** |  | [optional] 
**name** | **str** |  | [optional] 
**remind_days** | **str** |  | [optional] 
**remind_date** | **datetime** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

