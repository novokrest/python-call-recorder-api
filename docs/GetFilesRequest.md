# GetFilesRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**api_key** | **str** |  | 
**page** | **str** |  | [optional] 
**folder_id** | **int** |  | [optional] 
**source** | **str** |  | [optional] 
**_pass** | **str** |  | [optional] 
**reminder** | **bool** |  | [optional] 
**q** | **str** |  | [optional] 
**id** | **int** |  | [optional] 
**op** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

