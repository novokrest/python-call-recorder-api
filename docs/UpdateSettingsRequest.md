# UpdateSettingsRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**api_key** | **str** |  | 
**play_beep** | [**PlayBeep**](PlayBeep.md) |  | [optional] 
**files_permission** | [**FilesPermission**](FilesPermission.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

