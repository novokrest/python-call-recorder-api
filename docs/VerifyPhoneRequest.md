# VerifyPhoneRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**token** | **str** |  | 
**phone** | **str** |  | 
**code** | **str** |  | 
**mcc** | **str** |  | 
**app** | [**App**](App.md) |  | 
**device_token** | **str** |  | 
**device_id** | **str** |  | [optional] 
**device_type** | [**DeviceType**](DeviceType.md) |  | 
**time_zone** | **str** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

