# UpdateProfileRequestData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**f_name** | **str** |  | [optional] 
**l_name** | **str** |  | [optional] 
**email** | **str** |  | [optional] 
**is_public** | **bool** |  | [optional] 
**language** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

