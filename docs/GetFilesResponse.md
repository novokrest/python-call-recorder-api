# GetFilesResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | **str** |  | [optional] 
**credits** | **int** |  | [optional] 
**credits_trans** | **int** |  | [optional] 
**files** | [**list[GetFilesResponseFiles]**](GetFilesResponseFiles.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

