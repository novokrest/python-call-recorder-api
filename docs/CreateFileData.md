# CreateFileData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** |  | [optional] 
**email** | **str** |  | [optional] 
**phone** | **str** |  | [optional] 
**l_name** | **str** |  | [optional] 
**f_name** | **str** |  | [optional] 
**notes** | **str** |  | [optional] 
**tags** | **str** |  | [optional] 
**meta** | **list[str]** |  | [optional] 
**source** | **str** |  | [optional] 
**remind_days** | **str** |  | [optional] 
**remind_date** | **datetime** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

