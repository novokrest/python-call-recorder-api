# GetProfileResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | **str** |  | 
**code** | **str** |  | [optional] 
**profile** | [**GetProfileResponseProfile**](GetProfileResponseProfile.md) |  | [optional] 
**app** | [**App**](App.md) |  | [optional] 
**share_url** | **str** |  | [optional] 
**rate_url** | **str** |  | [optional] 
**credits** | **int** |  | [optional] 
**credits_trans** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

