# UpdateFolderRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**api_key** | **str** |  | 
**id** | **int** |  | [optional] 
**name** | **str** |  | [optional] 
**_pass** | **str** |  | [optional] 
**is_private** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

