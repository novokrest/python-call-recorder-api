# GetLanguagesResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | **str** |  | 
**msg** | **str** |  | [optional] 
**languages** | [**list[GetLanguagesResponseLanguages]**](GetLanguagesResponseLanguages.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

