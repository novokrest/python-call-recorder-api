# GetFoldersResponseFolders

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**name** | **str** |  | [optional] 
**created** | **int** |  | [optional] 
**updated** | **int** |  | [optional] 
**is_start** | **int** |  | [optional] 
**order_id** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

