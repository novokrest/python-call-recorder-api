# GetSettingsResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | **str** |  | 
**app** | [**App**](App.md) |  | [optional] 
**credits** | **int** |  | [optional] 
**settings** | [**GetSettingsResponseSettings**](GetSettingsResponseSettings.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

