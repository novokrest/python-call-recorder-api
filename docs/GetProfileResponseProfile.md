# GetProfileResponseProfile

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**f_name** | **str** |  | [optional] 
**l_name** | **str** |  | [optional] 
**email** | **str** |  | [optional] 
**phone** | **str** |  | [optional] 
**pic** | **str** |  | [optional] 
**language** | **str** |  | [optional] 
**is_public** | **int** |  | [optional] 
**play_beep** | **int** |  | [optional] 
**max_length** | **int** |  | [optional] 
**time_zone** | **str** |  | [optional] 
**time** | **int** |  | [optional] 
**pin** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

