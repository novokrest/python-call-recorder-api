# BuyCreditsRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**api_key** | **str** |  | 
**amount** | **int** |  | [optional] 
**receipt** | **str** |  | [optional] 
**product_id** | **int** |  | [optional] 
**device_type** | [**DeviceType**](DeviceType.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

