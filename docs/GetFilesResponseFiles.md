# GetFilesResponseFiles

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**access_number** | **str** |  | [optional] 
**name** | **str** |  | [optional] 
**f_name** | **str** |  | [optional] 
**l_name** | **str** |  | [optional] 
**email** | **str** |  | [optional] 
**phone** | **str** |  | [optional] 
**notes** | **str** |  | [optional] 
**meta** | **str** |  | [optional] 
**source** | **str** |  | [optional] 
**url** | **str** |  | [optional] 
**credits** | **str** |  | [optional] 
**duration** | **str** |  | [optional] 
**time** | **str** |  | [optional] 
**share_url** | **str** |  | [optional] 
**download_url** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

