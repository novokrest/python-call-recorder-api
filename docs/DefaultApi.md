# call_recorder_api.DefaultApi

All URIs are relative to *https://app2.virtualbrix.net/rapi*

Method | HTTP request | Description
------------- | ------------- | -------------
[**buy_credits_post**](DefaultApi.md#buy_credits_post) | **POST** /buy_credits | 
[**clone_file_post**](DefaultApi.md#clone_file_post) | **POST** /clone_file | 
[**create_file_post**](DefaultApi.md#create_file_post) | **POST** /create_file | 
[**create_folder_post**](DefaultApi.md#create_folder_post) | **POST** /create_folder | 
[**delete_files_post**](DefaultApi.md#delete_files_post) | **POST** /delete_files | 
[**delete_folder_post**](DefaultApi.md#delete_folder_post) | **POST** /delete_folder | 
[**delete_meta_files_post**](DefaultApi.md#delete_meta_files_post) | **POST** /delete_meta_files | 
[**get_files_post**](DefaultApi.md#get_files_post) | **POST** /get_files | 
[**get_folders_post**](DefaultApi.md#get_folders_post) | **POST** /get_folders | 
[**get_languages_post**](DefaultApi.md#get_languages_post) | **POST** /get_languages | 
[**get_meta_files_post**](DefaultApi.md#get_meta_files_post) | **POST** /get_meta_files | 
[**get_msgs_post**](DefaultApi.md#get_msgs_post) | **POST** /get_msgs | 
[**get_phones_post**](DefaultApi.md#get_phones_post) | **POST** /get_phones | 
[**get_profile_post**](DefaultApi.md#get_profile_post) | **POST** /get_profile | 
[**get_settings_post**](DefaultApi.md#get_settings_post) | **POST** /get_settings | 
[**get_translations_post**](DefaultApi.md#get_translations_post) | **POST** /get_translations | 
[**notify_user_custom_post**](DefaultApi.md#notify_user_custom_post) | **POST** /notify_user_custom | 
[**recover_file_post**](DefaultApi.md#recover_file_post) | **POST** /recover_file | 
[**register_phone_post**](DefaultApi.md#register_phone_post) | **POST** /register_phone | 
[**update_device_token_post**](DefaultApi.md#update_device_token_post) | **POST** /update_device_token | 
[**update_file_post**](DefaultApi.md#update_file_post) | **POST** /update_file | 
[**update_folder_post**](DefaultApi.md#update_folder_post) | **POST** /update_folder | 
[**update_order_post**](DefaultApi.md#update_order_post) | **POST** /update_order | 
[**update_profile_img_post**](DefaultApi.md#update_profile_img_post) | **POST** /update_profile_img | 
[**update_profile_post**](DefaultApi.md#update_profile_post) | **POST** /update_profile | 
[**update_settings_post**](DefaultApi.md#update_settings_post) | **POST** /update_settings | 
[**update_star_post**](DefaultApi.md#update_star_post) | **POST** /update_star | 
[**update_user_post**](DefaultApi.md#update_user_post) | **POST** /update_user | 
[**upload_meta_file_post**](DefaultApi.md#upload_meta_file_post) | **POST** /upload_meta_file | 
[**verify_folder_pass_post**](DefaultApi.md#verify_folder_pass_post) | **POST** /verify_folder_pass | 
[**verify_phone_post**](DefaultApi.md#verify_phone_post) | **POST** /verify_phone | 

# **buy_credits_post**
> BuyCreditsResponse buy_credits_post(api_key, amount, receipt, product_id, device_type)



Buy Credits 

### Example
```python
from __future__ import print_function
import time
import call_recorder_api
from call_recorder_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = call_recorder_api.DefaultApi()
api_key = 'api_key_example' # str | 
amount = 789 # int | 
receipt = 'receipt_example' # str | 
product_id = 789 # int | 
device_type = call_recorder_api.DeviceType() # DeviceType | 

try:
    api_response = api_instance.buy_credits_post(api_key, amount, receipt, product_id, device_type)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->buy_credits_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **api_key** | **str**|  | 
 **amount** | **int**|  | 
 **receipt** | **str**|  | 
 **product_id** | **int**|  | 
 **device_type** | [**DeviceType**](.md)|  | 

### Return type

[**BuyCreditsResponse**](BuyCreditsResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **clone_file_post**
> CloneFileResponse clone_file_post(api_key, id)



Clone File 

### Example
```python
from __future__ import print_function
import time
import call_recorder_api
from call_recorder_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = call_recorder_api.DefaultApi()
api_key = 'api_key_example' # str | 
id = 789 # int | 

try:
    api_response = api_instance.clone_file_post(api_key, id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->clone_file_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **api_key** | **str**|  | 
 **id** | **int**|  | 

### Return type

[**CloneFileResponse**](CloneFileResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_file_post**
> CreateFileResponse create_file_post(api_key, file, data)



Create File 

### Example
```python
from __future__ import print_function
import time
import call_recorder_api
from call_recorder_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = call_recorder_api.DefaultApi()
api_key = 'api_key_example' # str | 
file = 'file_example' # str | 
data = call_recorder_api.CreateFileData() # CreateFileData | 

try:
    api_response = api_instance.create_file_post(api_key, file, data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->create_file_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **api_key** | **str**|  | 
 **file** | **str**|  | 
 **data** | [**CreateFileData**](.md)|  | 

### Return type

[**CreateFileResponse**](CreateFileResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_folder_post**
> CreateFolderResponse create_folder_post(api_key, name, _pass)



Create Folder 

### Example
```python
from __future__ import print_function
import time
import call_recorder_api
from call_recorder_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = call_recorder_api.DefaultApi()
api_key = 'api_key_example' # str | 
name = 'name_example' # str | 
_pass = '_pass_example' # str | 

try:
    api_response = api_instance.create_folder_post(api_key, name, _pass)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->create_folder_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **api_key** | **str**|  | 
 **name** | **str**|  | 
 **_pass** | **str**|  | 

### Return type

[**CreateFolderResponse**](CreateFolderResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_files_post**
> DeleteFilesResponse delete_files_post(api_key, ids, action)



Delete Files 

### Example
```python
from __future__ import print_function
import time
import call_recorder_api
from call_recorder_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = call_recorder_api.DefaultApi()
api_key = 'api_key_example' # str | 
ids = [56] # list[int] | 
action = 'action_example' # str | 

try:
    api_response = api_instance.delete_files_post(api_key, ids, action)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->delete_files_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **api_key** | **str**|  | 
 **ids** | [**list[int]**](int.md)|  | 
 **action** | **str**|  | 

### Return type

[**DeleteFilesResponse**](DeleteFilesResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_folder_post**
> DeleteFolderResponse delete_folder_post(api_key, id, move_to)



Delete Folder 

### Example
```python
from __future__ import print_function
import time
import call_recorder_api
from call_recorder_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = call_recorder_api.DefaultApi()
api_key = 'api_key_example' # str | 
id = 789 # int | 
move_to = 789 # int | 

try:
    api_response = api_instance.delete_folder_post(api_key, id, move_to)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->delete_folder_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **api_key** | **str**|  | 
 **id** | **int**|  | 
 **move_to** | **int**|  | 

### Return type

[**DeleteFolderResponse**](DeleteFolderResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_meta_files_post**
> DeleteMetaFilesResponse delete_meta_files_post(api_key, ids, parent_id)



Delete Meta Files 

### Example
```python
from __future__ import print_function
import time
import call_recorder_api
from call_recorder_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = call_recorder_api.DefaultApi()
api_key = 'api_key_example' # str | 
ids = [56] # list[int] | 
parent_id = 789 # int | 

try:
    api_response = api_instance.delete_meta_files_post(api_key, ids, parent_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->delete_meta_files_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **api_key** | **str**|  | 
 **ids** | [**list[int]**](int.md)|  | 
 **parent_id** | **int**|  | 

### Return type

[**DeleteMetaFilesResponse**](DeleteMetaFilesResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_files_post**
> GetFilesResponse get_files_post(api_key, page, folder_id, source, _pass, reminder, q, id, op)



Get Files 

### Example
```python
from __future__ import print_function
import time
import call_recorder_api
from call_recorder_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = call_recorder_api.DefaultApi()
api_key = 'api_key_example' # str | 
page = 'page_example' # str | 
folder_id = 789 # int | 
source = 'source_example' # str | 
_pass = '_pass_example' # str | 
reminder = true # bool | 
q = 'q_example' # str | 
id = 789 # int | 
op = 'op_example' # str | 

try:
    api_response = api_instance.get_files_post(api_key, page, folder_id, source, _pass, reminder, q, id, op)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_files_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **api_key** | **str**|  | 
 **page** | **str**|  | 
 **folder_id** | **int**|  | 
 **source** | **str**|  | 
 **_pass** | **str**|  | 
 **reminder** | **bool**|  | 
 **q** | **str**|  | 
 **id** | **int**|  | 
 **op** | **str**|  | 

### Return type

[**GetFilesResponse**](GetFilesResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_folders_post**
> GetFoldersResponse get_folders_post(api_key)



Get Folders 

### Example
```python
from __future__ import print_function
import time
import call_recorder_api
from call_recorder_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = call_recorder_api.DefaultApi()
api_key = 'api_key_example' # str | 

try:
    api_response = api_instance.get_folders_post(api_key)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_folders_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **api_key** | **str**|  | 

### Return type

[**GetFoldersResponse**](GetFoldersResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_languages_post**
> GetLanguagesResponse get_languages_post(api_key)



Get Languages 

### Example
```python
from __future__ import print_function
import time
import call_recorder_api
from call_recorder_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = call_recorder_api.DefaultApi()
api_key = 'api_key_example' # str | 

try:
    api_response = api_instance.get_languages_post(api_key)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_languages_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **api_key** | **str**|  | 

### Return type

[**GetLanguagesResponse**](GetLanguagesResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_meta_files_post**
> GetMetaFilesResponse get_meta_files_post(api_key, parent_id)



Get Meta File 

### Example
```python
from __future__ import print_function
import time
import call_recorder_api
from call_recorder_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = call_recorder_api.DefaultApi()
api_key = 'api_key_example' # str | 
parent_id = 789 # int | 

try:
    api_response = api_instance.get_meta_files_post(api_key, parent_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_meta_files_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **api_key** | **str**|  | 
 **parent_id** | **int**|  | 

### Return type

[**GetMetaFilesResponse**](GetMetaFilesResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_msgs_post**
> GetMessagesResponse get_msgs_post(api_key)



Get Messages 

### Example
```python
from __future__ import print_function
import time
import call_recorder_api
from call_recorder_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = call_recorder_api.DefaultApi()
api_key = 'api_key_example' # str | 

try:
    api_response = api_instance.get_msgs_post(api_key)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_msgs_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **api_key** | **str**|  | 

### Return type

[**GetMessagesResponse**](GetMessagesResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_phones_post**
> GetPhonesResponse get_phones_post(api_key)



Get Phones 

### Example
```python
from __future__ import print_function
import time
import call_recorder_api
from call_recorder_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = call_recorder_api.DefaultApi()
api_key = 'api_key_example' # str | 

try:
    api_response = api_instance.get_phones_post(api_key)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_phones_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **api_key** | **str**|  | 

### Return type

[**GetPhonesResponse**](GetPhonesResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_profile_post**
> GetProfileResponse get_profile_post(api_key)



Get Profile 

### Example
```python
from __future__ import print_function
import time
import call_recorder_api
from call_recorder_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = call_recorder_api.DefaultApi()
api_key = 'api_key_example' # str | 

try:
    api_response = api_instance.get_profile_post(api_key)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_profile_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **api_key** | **str**|  | 

### Return type

[**GetProfileResponse**](GetProfileResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_settings_post**
> GetSettingsResponse get_settings_post(api_key)



Get Settings 

### Example
```python
from __future__ import print_function
import time
import call_recorder_api
from call_recorder_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = call_recorder_api.DefaultApi()
api_key = 'api_key_example' # str | 

try:
    api_response = api_instance.get_settings_post(api_key)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_settings_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **api_key** | **str**|  | 

### Return type

[**GetSettingsResponse**](GetSettingsResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_translations_post**
> GetTranslationsResponse get_translations_post(api_key, language)



Get Translations 

### Example
```python
from __future__ import print_function
import time
import call_recorder_api
from call_recorder_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = call_recorder_api.DefaultApi()
api_key = 'api_key_example' # str | 
language = 'language_example' # str | 

try:
    api_response = api_instance.get_translations_post(api_key, language)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_translations_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **api_key** | **str**|  | 
 **language** | **str**|  | 

### Return type

[**GetTranslationsResponse**](GetTranslationsResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **notify_user_custom_post**
> NotifyUserResponse notify_user_custom_post(api_key, title, body, device_type)



Notify User 

### Example
```python
from __future__ import print_function
import time
import call_recorder_api
from call_recorder_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = call_recorder_api.DefaultApi()
api_key = 'api_key_example' # str | 
title = 'title_example' # str | 
body = 'body_example' # str | 
device_type = call_recorder_api.DeviceType() # DeviceType | 

try:
    api_response = api_instance.notify_user_custom_post(api_key, title, body, device_type)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->notify_user_custom_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **api_key** | **str**|  | 
 **title** | **str**|  | 
 **body** | **str**|  | 
 **device_type** | [**DeviceType**](.md)|  | 

### Return type

[**NotifyUserResponse**](NotifyUserResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **recover_file_post**
> RecoverFileResponse recover_file_post(api_key, id, folder_id)



Recover File 

### Example
```python
from __future__ import print_function
import time
import call_recorder_api
from call_recorder_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = call_recorder_api.DefaultApi()
api_key = 'api_key_example' # str | 
id = 789 # int | 
folder_id = 789 # int | 

try:
    api_response = api_instance.recover_file_post(api_key, id, folder_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->recover_file_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **api_key** | **str**|  | 
 **id** | **int**|  | 
 **folder_id** | **int**|  | 

### Return type

[**RecoverFileResponse**](RecoverFileResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **register_phone_post**
> RegisterPhoneResponse register_phone_post(token, phone)



Register Phone, Send phone number to server to get verification code  

### Example
```python
from __future__ import print_function
import time
import call_recorder_api
from call_recorder_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = call_recorder_api.DefaultApi()
token = 'token_example' # str | 
phone = 'phone_example' # str | 

try:
    api_response = api_instance.register_phone_post(token, phone)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->register_phone_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **token** | **str**|  | 
 **phone** | **str**|  | 

### Return type

[**RegisterPhoneResponse**](RegisterPhoneResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_device_token_post**
> UpdateDeviceTokenResponse update_device_token_post(api_key, device_token, device_type)



Update Device Token 

### Example
```python
from __future__ import print_function
import time
import call_recorder_api
from call_recorder_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = call_recorder_api.DefaultApi()
api_key = 'api_key_example' # str | 
device_token = 'device_token_example' # str | 
device_type = call_recorder_api.DeviceType() # DeviceType | 

try:
    api_response = api_instance.update_device_token_post(api_key, device_token, device_type)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->update_device_token_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **api_key** | **str**|  | 
 **device_token** | **str**|  | 
 **device_type** | [**DeviceType**](.md)|  | 

### Return type

[**UpdateDeviceTokenResponse**](UpdateDeviceTokenResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_file_post**
> UpdateFileResponse update_file_post(api_key, id, f_name, l_name, notes, email, phone, tags, folder_id, name, remind_days, remind_date)



Update File 

### Example
```python
from __future__ import print_function
import time
import call_recorder_api
from call_recorder_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = call_recorder_api.DefaultApi()
api_key = 'api_key_example' # str | 
id = 789 # int | 
f_name = 'f_name_example' # str | 
l_name = 'l_name_example' # str | 
notes = 'notes_example' # str | 
email = 'email_example' # str | 
phone = 'phone_example' # str | 
tags = 'tags_example' # str | 
folder_id = 789 # int | 
name = 'name_example' # str | 
remind_days = 'remind_days_example' # str | 
remind_date = '2013-10-20T19:20:30+01:00' # datetime | 

try:
    api_response = api_instance.update_file_post(api_key, id, f_name, l_name, notes, email, phone, tags, folder_id, name, remind_days, remind_date)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->update_file_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **api_key** | **str**|  | 
 **id** | **int**|  | 
 **f_name** | **str**|  | 
 **l_name** | **str**|  | 
 **notes** | **str**|  | 
 **email** | **str**|  | 
 **phone** | **str**|  | 
 **tags** | **str**|  | 
 **folder_id** | **int**|  | 
 **name** | **str**|  | 
 **remind_days** | **str**|  | 
 **remind_date** | **datetime**|  | 

### Return type

[**UpdateFileResponse**](UpdateFileResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_folder_post**
> UpdateFolderResponse update_folder_post(api_key, id, name, _pass, is_private)



Update Folder 

### Example
```python
from __future__ import print_function
import time
import call_recorder_api
from call_recorder_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = call_recorder_api.DefaultApi()
api_key = 'api_key_example' # str | 
id = 789 # int | 
name = 'name_example' # str | 
_pass = '_pass_example' # str | 
is_private = true # bool | 

try:
    api_response = api_instance.update_folder_post(api_key, id, name, _pass, is_private)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->update_folder_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **api_key** | **str**|  | 
 **id** | **int**|  | 
 **name** | **str**|  | 
 **_pass** | **str**|  | 
 **is_private** | **bool**|  | 

### Return type

[**UpdateFolderResponse**](UpdateFolderResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_order_post**
> UpdateOrderResponse update_order_post(api_key, folders)



Update Order 

### Example
```python
from __future__ import print_function
import time
import call_recorder_api
from call_recorder_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = call_recorder_api.DefaultApi()
api_key = 'api_key_example' # str | 
folders = [call_recorder_api.UpdateOrderRequestFolders()] # list[UpdateOrderRequestFolders] | 

try:
    api_response = api_instance.update_order_post(api_key, folders)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->update_order_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **api_key** | **str**|  | 
 **folders** | [**list[UpdateOrderRequestFolders]**](UpdateOrderRequestFolders.md)|  | 

### Return type

[**UpdateOrderResponse**](UpdateOrderResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_profile_img_post**
> UpdateProfileImgResponse update_profile_img_post(api_key, file)



Update Profile Img 

### Example
```python
from __future__ import print_function
import time
import call_recorder_api
from call_recorder_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = call_recorder_api.DefaultApi()
api_key = 'api_key_example' # str | 
file = 'file_example' # str | 

try:
    api_response = api_instance.update_profile_img_post(api_key, file)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->update_profile_img_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **api_key** | **str**|  | 
 **file** | **str**|  | 

### Return type

[**UpdateProfileImgResponse**](UpdateProfileImgResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_profile_post**
> UpdateProfileResponse update_profile_post(api_key, data)



Update Profile 

### Example
```python
from __future__ import print_function
import time
import call_recorder_api
from call_recorder_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = call_recorder_api.DefaultApi()
api_key = 'api_key_example' # str | 
data = call_recorder_api.UpdateProfileRequestData() # UpdateProfileRequestData | 

try:
    api_response = api_instance.update_profile_post(api_key, data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->update_profile_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **api_key** | **str**|  | 
 **data** | [**UpdateProfileRequestData**](.md)|  | 

### Return type

[**UpdateProfileResponse**](UpdateProfileResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_settings_post**
> UpdateSettingsResponse update_settings_post(api_key, play_beep, files_permission)



Update Settings 

### Example
```python
from __future__ import print_function
import time
import call_recorder_api
from call_recorder_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = call_recorder_api.DefaultApi()
api_key = 'api_key_example' # str | 
play_beep = call_recorder_api.PlayBeep() # PlayBeep | 
files_permission = call_recorder_api.FilesPermission() # FilesPermission | 

try:
    api_response = api_instance.update_settings_post(api_key, play_beep, files_permission)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->update_settings_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **api_key** | **str**|  | 
 **play_beep** | [**PlayBeep**](.md)|  | 
 **files_permission** | [**FilesPermission**](.md)|  | 

### Return type

[**UpdateSettingsResponse**](UpdateSettingsResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_star_post**
> UpdateStarResponse update_star_post(api_key, id, star, type)



Update Star 

### Example
```python
from __future__ import print_function
import time
import call_recorder_api
from call_recorder_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = call_recorder_api.DefaultApi()
api_key = 'api_key_example' # str | 
id = 789 # int | 
star = 56 # int | 
type = 'type_example' # str | 

try:
    api_response = api_instance.update_star_post(api_key, id, star, type)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->update_star_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **api_key** | **str**|  | 
 **id** | **int**|  | 
 **star** | **int**|  | 
 **type** | **str**|  | 

### Return type

[**UpdateStarResponse**](UpdateStarResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_user_post**
> UpdateUserResponse update_user_post(api_key, app, timezone)



Update User 

### Example
```python
from __future__ import print_function
import time
import call_recorder_api
from call_recorder_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = call_recorder_api.DefaultApi()
api_key = 'api_key_example' # str | 
app = call_recorder_api.App() # App | 
timezone = 'timezone_example' # str | 

try:
    api_response = api_instance.update_user_post(api_key, app, timezone)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->update_user_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **api_key** | **str**|  | 
 **app** | [**App**](.md)|  | 
 **timezone** | **str**|  | 

### Return type

[**UpdateUserResponse**](UpdateUserResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **upload_meta_file_post**
> UploadMetaFileResponse upload_meta_file_post(api_key, file, name, parent_id, id)



Upload Meta File 

### Example
```python
from __future__ import print_function
import time
import call_recorder_api
from call_recorder_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = call_recorder_api.DefaultApi()
api_key = 'api_key_example' # str | 
file = 'file_example' # str | 
name = 'name_example' # str | 
parent_id = 789 # int | 
id = 789 # int | 

try:
    api_response = api_instance.upload_meta_file_post(api_key, file, name, parent_id, id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->upload_meta_file_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **api_key** | **str**|  | 
 **file** | **str**|  | 
 **name** | **str**|  | 
 **parent_id** | **int**|  | 
 **id** | **int**|  | 

### Return type

[**UploadMetaFileResponse**](UploadMetaFileResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **verify_folder_pass_post**
> VerifyFolderPassResponse verify_folder_pass_post(api_key, id, _pass)



Verify Folder Pass 

### Example
```python
from __future__ import print_function
import time
import call_recorder_api
from call_recorder_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = call_recorder_api.DefaultApi()
api_key = 'api_key_example' # str | 
id = 789 # int | 
_pass = '_pass_example' # str | 

try:
    api_response = api_instance.verify_folder_pass_post(api_key, id, _pass)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->verify_folder_pass_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **api_key** | **str**|  | 
 **id** | **int**|  | 
 **_pass** | **str**|  | 

### Return type

[**VerifyFolderPassResponse**](VerifyFolderPassResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **verify_phone_post**
> VerifyPhoneResponse verify_phone_post(token, phone, code, mcc, app, device_token, device_id, device_type, time_zone)



Send phone number and verification code to get API Key 

### Example
```python
from __future__ import print_function
import time
import call_recorder_api
from call_recorder_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = call_recorder_api.DefaultApi()
token = 'token_example' # str | 
phone = 'phone_example' # str | 
code = 'code_example' # str | 
mcc = 'mcc_example' # str | 
app = call_recorder_api.App() # App | 
device_token = 'device_token_example' # str | 
device_id = 'device_id_example' # str | 
device_type = call_recorder_api.DeviceType() # DeviceType | 
time_zone = 'time_zone_example' # str | 

try:
    api_response = api_instance.verify_phone_post(token, phone, code, mcc, app, device_token, device_id, device_type, time_zone)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->verify_phone_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **token** | **str**|  | 
 **phone** | **str**|  | 
 **code** | **str**|  | 
 **mcc** | **str**|  | 
 **app** | [**App**](.md)|  | 
 **device_token** | **str**|  | 
 **device_id** | **str**|  | 
 **device_type** | [**DeviceType**](.md)|  | 
 **time_zone** | **str**|  | 

### Return type

[**VerifyPhoneResponse**](VerifyPhoneResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

